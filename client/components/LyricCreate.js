import React, { Component } from 'react';
import gql from 'graphql-tag'
import { graphql } from 'react-apollo';

class LyricCreate extends Component {
  constructor(props){
    super(props)
    this.state = { content: '' }
  }
  onSubmit(event){
    event.preventDefault();
    this.props.mutate({ 
      variables: { 
        content: this.state.content,
        songId: this.props.songId
      },
      refetchQueries: []
    }).then(() => this.setState({ content: '' }))
  }
  render(){
    return(
      <form onSubmit={this.onSubmit.bind(this)}>
        <label>Add a lyrics</label>
        <input
          value={this.state.content}
          onChange={event => this.setState({ content: event.target.value})}
        />
      </form>
    )
  }
}

const mutation = gql`
  mutation AddLyricsToSong($content: String, $songId: ID){
    addLyricToSong(songId: $songId, content: $content){
      id
      title
      lyrics{
        id
        content
        likes
      }
    }
  }
`;

export default graphql(mutation)(LyricCreate);
